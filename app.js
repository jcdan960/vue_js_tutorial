const app = Vue.createApp({
    data() {
        return {
            firstName: 'John',
            lastName: 'Doe',
            email: 'johndoe@gmail.com',
            gender:'male',
            picture:'https://randomuser.me/api/portraits/men/10.jpg'
        }
    },
    methods: {
        async getUser(){
            const res = await fetch('https://randomuser.me/api')
            const {results} = await res.json()
            console.log(results)

            this.firstName = "JC"
            this.lastName = "Dan"
            this.email = "jc@gmail.com"
            this.gender = "male"
            console.log(this.firstName)
        },    
    }
})

app.mount('#app')